# Terraform Lab Setup
This stands as a working example of a terraform provisioned lab.
Contents will be mostly focused on configuring a proper CI/CD 
environment as well as focusing on immutable infrastructure such
as container linux. 

# Required Terraform plugins

## For using templates
https://github.com/terraform-providers/terraform-provider-template

## For libvirt
https://github.com/dmacvicar/terraform-provider-libvirt

# To Do:
I need to configure chef provisioning for the "lab" servers. These
boxes are currently only deployed and not provisioned. 
