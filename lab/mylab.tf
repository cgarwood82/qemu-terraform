variable "server_names" {
  default = {
    "0" = "metrics.garwood.local"
    "1" = "splunk.garwood.local"
    "2" = "adam.garwood.local"
  }
}

variable "mylab_macs" {
  default = {
    "0" = "00:16:3e:1e:2b:82"
    "1" = "00:16:3e:06:ff:f7"
    "2" = "00:16:3e:06:a0:0f"
  }
}


provider "libvirt" {
    uri = "qemu+ssh://trans@virt.garwood.local/system"
}

resource "libvirt_volume" "lab_image" {
  name = "lab_image"
  source = "http://web.garwood.local/packer-centos-7-server-x86_64.qcow2"
}

resource "libvirt_volume" "storage" {
  count = "3"
  name   = "${lookup(var.server_names, count.index)}-storage.qcow2"
  base_volume_id = "${libvirt_volume.lab_image.id}"
}

resource "libvirt_domain" "metrics" {
  name   = "metrics.garwood.local"
  memory   = "2048"
  vcpu = 2

  disk {
    volume_id = "${libvirt_volume.storage.*.id[0]}"
  }

  network_interface {
    bridge = "br0"
    mac = "${lookup(var.mylab_macs, 0)}"
  }

  provisioner "chef" {
    node_name = "${lookup(var.server_names, 0)}"
    run_list = ["cigii-lab"]
    server_url      = "https://chef.garwood.local/organizations/mylab"
    recreate_client = true
    user_name       = "trans"
    user_key        = "${file("~/.chef/cgarwood.pem")}"
    ssl_verify_mode = ":verify_peer"
    fetch_chef_certificates = true
  }

  connection {
    type = "ssh"
    user = "root"
    password = "SomePassword"
    host = "${lookup(var.server_names, 0)}"
  }
}

resource "libvirt_domain" "splunk" {

  name   = "splunk.garwood.local"
  memory   = "2048"
  vcpu = 2

  disk {
    volume_id = "${libvirt_volume.storage.*.id[1]}"
  }

  network_interface {
    bridge = "br0"
    mac = "${lookup(var.mylab_macs, 1)}"
  }

  provisioner "chef" {
    node_name = "${lookup(var.server_names, 1)}"
    run_list = ["cigii-lab"]
    server_url      = "https://chef.garwood.local/organizations/mylab"
    recreate_client = true
    user_name       = "trans"
    user_key        = "${file("~/.chef/cgarwood.pem")}"
    ssl_verify_mode = ":verify_peer"
    fetch_chef_certificates = true
  }

  connection {
    type = "ssh"
    user = "root"
    password = "SomePassword"
    host = "${lookup(var.server_names, 1)}"
  }
}

resource "libvirt_domain" "adam" {

  name   = "adam.garwood.local"
  memory   = "2048"
  vcpu = 2

  disk {
    volume_id = "${libvirt_volume.storage.*.id[2]}"
  }

  network_interface {
    bridge = "br0"
    mac = "${lookup(var.mylab_macs, 2)}"
  }

  provisioner "chef" {
    node_name = "${lookup(var.server_names, 2)}"
    run_list = ["cigii-lab"]
    server_url      = "https://chef.garwood.local/organizations/mylab"
    recreate_client = true
    user_name       = "trans"
    user_key        = "${file("~/.chef/cgarwood.pem")}"
    ssl_verify_mode = ":verify_peer"
    fetch_chef_certificates = true
  }

  connection {
    type = "ssh"
    user = "root"
    password = "SomePassword"
    host = "${lookup(var.server_names, 2)}"
  }
}
