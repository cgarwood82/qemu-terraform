variable "server_names" {
  default = {
    "0" = "jenkins.garwood.local"
  }
}

variable "mylab_macs" {
  default = {
    "0" = "00:16:3e:37:72:03"
  }
}

provider "libvirt" {
    uri = "qemu+ssh://trans@virt.garwood.local/system"
}

resource "libvirt_volume" "lab_image" {
  name = "lab_image_jenkins"
  source = "http://web.garwood.local/packer-centos-7-server-x86_64.qcow2"
}

resource "libvirt_volume" "storage" {
  count = "1"
  name   = "${lookup(var.server_names, count.index)}-storage.qcow2"
  base_volume_id = "${libvirt_volume.lab_image.id}"
}

resource "libvirt_domain" "jenkins" {
  name   = "jenkins.garwood.local"
  memory   = "4096"
  vcpu = 4

  disk {
    volume_id = "${libvirt_volume.storage.*.id[0]}"
  }

  network_interface {
    bridge = "br0"
    mac = "${lookup(var.mylab_macs, 0)}"
  }

  provisioner "chef" {
    attributes_json = <<-EOF
      {
        "jenkins": {
          "master": {
            "install_method": "package"
          }
        },
        "java": {
          "jdk_version": "8"
        }
      }
    EOF

    node_name = "${lookup(var.server_names, 0)}"
    run_list = ["java", "cigii_firewall", "cigii_firewall::jenkins", "cigii-lab", "cigii_jenkins"]
    server_url      = "https://chef.garwood.local/organizations/mylab"
    recreate_client = true
    user_name       = "trans"
    user_key        = "${file("~/.chef/cgarwood.pem")}"
    ssl_verify_mode = ":verify_peer"
    fetch_chef_certificates = true
  }

  connection {
    type = "ssh"
    user = "root"
    password = "SomePassword"
    host = "${lookup(var.server_names, 0)}"
  }
}
