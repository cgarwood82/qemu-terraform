provider "libvirt" {
    uri = "qemu+ssh://trans@virt.garwood.local/system"
}

variable "coreos_names" {
  default = {
    "0" = "core001.garwood.local"
    "1" = "core002.garwood.local"
    "2" = "core003.garwood.local"
    "3" = "core004.garwood.local"
  }
}

resource "libvirt_ignition" "ignition" {
  count = "3"
  name = "ignition${count.index}"
  content = "${file("${path.module}/config${count.index + 1}.ign")}"
}

resource "libvirt_volume" "coreos_image" {
  name = "coreos_image"
  source = "http://web.garwood.local/coreos_production_qemu_image.img"
}

resource "libvirt_volume" "coreos_storage" {
  count = "3"
  name = "${lookup(var.coreos_names, count.index)}-storage.qcow2"
  base_volume_id = "${libvirt_volume.coreos_image.id}"
}

resource "libvirt_domain" "coreos_server" {
  count = "3"
  name = "${lookup(var.coreos_names, count.index)}"
  memory = "2048"

  coreos_ignition = "${libvirt_ignition.ignition.*.id[count.index]}"

  disk {
    volume_id = "${libvirt_volume.coreos_storage.*.id[count.index]}"
  }

  network_interface {
    bridge = "br0"
  }

}
